package io.robustperception.java_examples;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

import io.prometheus.client.Counter;
import io.prometheus.client.exporter.MetricsServlet;
import io.prometheus.client.hotspot.DefaultExports;

public class MetricsApplication {

	static final Counter requests = Counter.build().name("Total").help("Number of Request Recievedserved.").register();
	static final Counter exrequests = Counter.build().name("Exception").help("Number of Exceptions  In My Servlet.").register();

	public static class MyHelloWorld extends HttpServlet {
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
			try {
				resp.getWriter().write("Hello");
				requests.inc();
			} catch (Exception e) {
				exrequests.inc();
			}
		}
	}

	public static void main(String[] args) throws Exception {
		// starting a tomcat-->
		Server server = createMetricAgent();
		DefaultExports.initialize();
		server.start();
		server.join();
	}

	private static Server createMetricAgent() {
		Server server = new Server(9191);
		ServletContextHandler handler = new ServletContextHandler();
		handler.setContextPath("/");
		server.setHandler(handler);
		handler.addServlet(new ServletHolder(new MetricsServlet()), "/metrics");
		handler.addServlet(new ServletHolder(new MyHelloWorld()), "/hello");
		return server;
	}
}
