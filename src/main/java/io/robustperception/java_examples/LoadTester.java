package io.robustperception.java_examples;

import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;

public class LoadTester {

	public static void main(String[] args) throws Exception {

		ExecutorService service = Executors.newFixedThreadPool(250);
		for (int i = 0; i < 100; i++) {

			service.execute(new Runnable() {
				@Override
				public void run() {
					for (int j = 0; j < 10; j++) {
						try {
							URL url = new URL("https://www.vinsysit.com:8443");
							HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
							InputStream is = connection.getInputStream();
							byte b[] = new byte[is.available()];
							is.read(b);
						} catch (Exception e) {
							e.printStackTrace();
						}

					}
				}
			});
		}
		service.shutdown();
	}
}
